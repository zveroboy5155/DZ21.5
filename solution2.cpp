//
// Created by german on 26.04.2022.
//

#include "solution2.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>

using namespace std;

enum OBJECT_TYPE{
    ROOM,
    FLOOR,
    BUILDING,
    PLOT,
    STREET,
};
string objet_name[] ={
    "о комнате",
    "об этаж",
    "о строение",
    "об участке",
    "об улице",
    "о селе"
};

enum BUILDING_TYPE{
    HOUSE,
    GARAGE,
    BATH,
    ALCOVE
};
string build_name[] = {
    "Жилой дом",
    "Гараж",
    "Баня",
    "Беседка",
};

enum ROOM_TYPE{
    BEDROOM,
    BATHROOM,
    KITCHEN,
    LIVING_ROOM,
    CHILDREN_ROOM
};
string room_name[] = {
        "спальной комнаты",
        "ванной комнаты",
        "кухни",
        "гостинной",
        "детской комнаты"
};
struct room{
    int width, length, height;
    int bed_number;
    ROOM_TYPE appointment = BEDROOM;
};

struct build_floor{
    int num;
    int room_num;
    int window_num;
    int ceiling_height;
    vector<room> rooms;
};

struct building{
    BUILDING_TYPE type;
    int width, length, height;
    double area;
    vector<build_floor> build_floors;
    int people_number;
    bool stove;
};

struct plot{
    int width, length, height_of_sea;
    vector <building> buildings;
};

struct street{
    string name;
    vector<plot> plots;
    bool night_light;
    int lamp_num;
};

struct village{
    vector<street> streeets;
    string name;
    int usualy_people_number = 10, max_people_number = 20;
};

void print_object(void * list, OBJECT_TYPE type){
    vector<street> streets;//вектор улиц
    vector<plot> plots; //вектор участков
    vector <building> buildings; //вектор зданий
    vector<build_floor> build_floors;// вектор этажей*/
    vector<room> rooms; //вектор комнат
    int len = 0;
    string tabs = "   ";

    switch (type) {
        case ROOM:
            tabs = tabs + tabs + tabs + tabs + tabs + tabs;
            rooms = *(vector<room>*)list;
            len = rooms.size();
            break;
        case FLOOR:
            tabs = tabs + tabs + tabs + tabs + tabs;
            build_floors = *(vector<build_floor>*)list;
            len = build_floors.size();
            break;
        case BUILDING:
            tabs = tabs + tabs + tabs + tabs;
            buildings = *(vector<building>*)list;
            len = buildings.size();
            break;

        case PLOT:
            tabs = tabs + tabs + tabs;
            plots = *(vector<plot>*)list;
            len = plots.size();
            break;

        case STREET:
            tabs = tabs + tabs;
            streets = *(vector<street>*)list;
            len = streets.size();
            break;

        default:
            cout<<"Ошибка типа вектора для печати :" << type << endl;
            return;
    }
    cout << tabs << "Информация " << objet_name[type] << endl;

    for(int i = 0; i < len; i++ ){
        switch (type) {
            case ROOM:
                cout << tabs << "Размеры  " << room_name[rooms[i].appointment] << " (ширина, длинна, высота потолков):"
                << rooms[i].width << " " << rooms[i].length << " " << rooms[i].height << endl;
                cout << tabs << "Количество кроватей в комнате " << rooms[i].bed_number << endl;
                continue;
            case FLOOR:
                cout << tabs << "На этаже № " << build_floors[i].num << endl;
                cout << tabs << build_floors[i].room_num << " комнат" << endl;
                cout << tabs << build_floors[i].window_num << " окон" << endl;
                cout << tabs << "Высота пололков " << build_floors[i].ceiling_height << endl;
                if(build_floors[i].rooms.size() > 0) print_object(&build_floors[i].rooms, ROOM);
                continue;
            case BUILDING:
                //cout << tabs << build_name[buildings[i].type] << endl;
                cout << tabs <<  "Размеры строения \" " << build_name[buildings[i].type] << "\": " << buildings[i].width << " " << buildings[i].length << " " << buildings[i].height << endl;
                cout << tabs <<  "Общей площадью " << buildings[i].area << endl;
                cout << tabs << ((buildings[i].stove) ? "Печка есть" : "Печки нет") << endl;

                if(buildings[i].build_floors.size() > 0) print_object(&buildings[i].build_floors, FLOOR);
                continue;
            case PLOT:
                cout << tabs << "Размеры участка (ширина, длинна, высота над уровнем моря): " << plots[i].width << " " << plots[i].length << " " << plots[i].height_of_sea << endl;
                if(plots[i].buildings.size() > 0) print_object(&plots[i].buildings, BUILDING);
                continue;
            case STREET:
                cout << tabs << "На улице " <<  streets[i].name << endl;
                cout << tabs << streets[i].lamp_num << " ламп " << ((streets[i].night_light) ? " и есть ночное освящение" : " и нет ночного освящения") << endl;
                if(streets[i].plots.size() > 0) print_object(&streets[i].plots, PLOT);
                break;
        }
    }
}

void print_info(village &cur_vil){
    string tab = "   ";

    cout << "Информация о селе " << cur_vil.name << endl;
    cout << tab << "Количество улиц: " << cur_vil.streeets.size() << endl;
    cout << tab << "Количество постоянно живущих людей: " << cur_vil.usualy_people_number << endl;
    cout << tab << "Максимальной количество людей в посёлке " << cur_vil.max_people_number << endl;

    print_object(&cur_vil.streeets, STREET);
}

string street_name[] = {
    "Садовая",
    "Вишнёвая",
    "Виноградная",
    "Дружбы",
    "Весны",
};


village make_default(){
    village def_vil;

     for( int i = 0; i < 5; i++){
         street new_street;
         new_street.name = street_name[i];

         for( int j = 0 ; j < 5; j++){
             plot new_plot;
             new_plot.length = (rand() % 500) + 200;
             new_plot.width = (rand() % 500) + 200;
             new_plot.height_of_sea = (rand() % 300) + 200;

             for( int k = 0; k < 5; k++){
                 building new_building;
                 new_building.type = (BUILDING_TYPE)(rand() % 4);
                 new_building.length = new_plot.length / 6;
                 new_building.width = new_plot.width / 6;
                 new_building.height = (rand() % 6) + 4;
                 for(int fl_num = 0; fl_num < 3; fl_num++){
                     build_floor new_build_floor;
                     new_build_floor.num = fl_num;

                     for(int room_num = 0; room_num < 3; room_num++){
                         room new_room;
                         new_room.height = (float)new_building.height /3.5;
                         new_room.width = (float)new_building.width /3.2;
                         new_room.length = (float)new_building.length /3.2;
                         new_room.bed_number = rand() % 4 + 1;
                         new_room.appointment = (ROOM_TYPE) (rand() % 5);
                         new_build_floor.rooms.push_back(new_room);
                     }
                     new_building.build_floors.push_back(new_build_floor);
                 }
                 new_plot.buildings.push_back(new_building);
             }
             new_street.plots.push_back(new_plot);
         }
         def_vil.streeets.push_back(new_street);
     }
    return def_vil;
}

bool add_object(OBJECT_TYPE type, void *out){
    string answer;
    int tmp_int;
    village tmp_village;
    street tmp_street;
    plot tmp_plot;
    building tmp_build;
    build_floor tmp_build_floor;
    room tmp_room;

    cout<< "Хотите добавить " << objet_name[type]  << " в " << objet_name[type + 1]<< " (да/нет)" << endl;
    cin >> answer;
    if(answer != "да") return NULL;
    //cout << "Check3!!!!!!" << endl;

    switch (type){
        case STREET:
            cout << "Введите название улицы" << endl;
            cin >> tmp_street.name;
            cout << "Введите количество ламп на улице и наличие ночного света(да/нет)" << endl;
            cin >> tmp_street.lamp_num >> answer;
            tmp_street.night_light = (answer == "да");

            add_object(PLOT, &tmp_street);
            (*(village*)out).streeets.push_back(tmp_street);
            if(add_object(STREET, &tmp_village)){
                //cout << "Печать tmp_village" << endl;
                //print_object(&tmp_village.streeets, STREET);
                (*(village*)out).streeets.insert((*(village*)out).streeets.end(), tmp_village.streeets.begin(), tmp_village.streeets.end());
                //cout << "закончена печать" << endl;
            }
            else return true;
            break;
        case PLOT:
            cout<< "Введите размеры участка (ширину, длинну, высоту над уровнем моря)" << endl;
            cin >> tmp_plot.width >> tmp_plot.length >> tmp_plot.height_of_sea;
            add_object(BUILDING, &tmp_plot);
            (*(street*)out).plots.push_back(tmp_plot);
            if(add_object(PLOT, &tmp_street))
                (*(street*)out).plots.insert((*(street*)out).plots.end(), tmp_street.plots.begin(), tmp_street.plots.end());
            else return false;
            break;
        case BUILDING:
            cout<< "Размеры участка " << (*(plot*)out).width << " " << (*(plot*)out).length << endl;
            cout << "Введите тип здания( 1 - 4)" << endl;
            cin >> tmp_int;
            tmp_build.type = (BUILDING_TYPE)(tmp_int - 1);
            cout<< "Размеры участка " << (*(plot*)out).width << " " << (*(plot*)out).length << endl;
            cout<< "Введите размеры (ширину, длинну, высоту) " << build_name[tmp_build.type] << endl;
            cin >> tmp_build.length >> tmp_build.width >> tmp_build.height;
            tmp_build.area = tmp_build.length * tmp_build.width * tmp_build.build_floors.size();

            if(tmp_build.type == HOUSE){
                cout << "Введите максимальное число жителей в доме" << endl;
                cin >> tmp_build.people_number;
                cout << "В доме есть печка?(да/нет)" << endl;
                cin >> answer;
                tmp_build.stove = ( answer == "да");
                add_object(FLOOR, &tmp_build);
            }
            (*(plot*)out).buildings.push_back(tmp_build);

            if(add_object(BUILDING, &tmp_plot))
                (*(plot*)out).buildings.insert((*(plot*)out).buildings.end(), tmp_plot.buildings.begin(), tmp_plot.buildings.end());
            else return false;
            break;
        case FLOOR:
            tmp_build_floor.num = (*(building*)out).build_floors.size();
            cout << "введите количество окон в коридоре" << endl;
            cin >> tmp_build_floor.window_num;
            add_object(ROOM, &tmp_build_floor);
            tmp_build_floor.room_num = tmp_build_floor.rooms.size();
            if (tmp_build_floor.num > 0 ) tmp_build_floor.ceiling_height = tmp_build_floor.rooms[0].height;
            else{
                cout << "Введите высоту потолков" << endl;
                cin >> tmp_build_floor.ceiling_height;
            }
            (*(building*)out).build_floors.push_back(tmp_build_floor);
            if(add_object(FLOOR, &tmp_build))
                (*(building*)out).build_floors.insert((*(building*)out).build_floors.end(), tmp_build.build_floors.begin(), tmp_build.build_floors.end());
            else return false;
            break;
        case ROOM:
            cout << "Введите назначение комнаты(1 - cпальня, 2 - ванная, 3 - кухня, 4 - гостинная,5 - детская)" << endl;
            cin >> tmp_int;
            tmp_room.appointment = (ROOM_TYPE)(tmp_int - 1);
            cout << "Введите размеры комнаты (ширина, длинна, высота потолков)" << endl;
            cin >> tmp_room.width >> tmp_room.length >> tmp_room.height;
            cout << "Введите количество кроватей в комнате" << endl;
            cin >> tmp_room.bed_number;
            (*(build_floor*)out).rooms.push_back(tmp_room);
            if(add_object(ROOM, &tmp_build_floor))
                (*(build_floor*)out).rooms.insert((*(build_floor*)out).rooms.end(), tmp_build_floor.rooms.begin(), tmp_build_floor.rooms.end());
            else return false;
            break;
    }
    return true;
}

village fill_info(){
    village my_village;
    int buff;

    cout << "Введите название посёлка" << endl;
    cin >> my_village.name;

    add_object(STREET, &my_village);
    cout << "Введите процент постоянно проживающих жителей(целый) от максимального числа жителей" << endl;
    cin >> buff;
    for(int i = 0; i < my_village.streeets.size(); i++){
        for(int j = 0; j < my_village.streeets[i].plots.size(); j++){
            for(int k = 0; k < my_village.streeets[i].plots[j].buildings.size(); k++){
                my_village.max_people_number += my_village.streeets[i].plots[j].buildings[k].people_number;
            }
        }
    }
    my_village.usualy_people_number = my_village.max_people_number/100 * buff;

    return my_village;
}

void sol2(){
    //village my_vil = make_default();
    village my_vil = fill_info();
    print_info(my_vil);

}
