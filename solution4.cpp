//
// Created by german on 14.05.2022.
//

#include "solution4.h"
#include <iostream>
#include <string>
#include <string.h>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <strstream>
#include <sstream>
#include <fstream>


enum DIRECTION{
    LEFT,
    RIGHT,
    TOP,
    BOTTOM
};

struct my_vec{
    int x, y;
};

struct character{
    std::string name;
    int health, armor;
    int damage;
    bool enemy;
    my_vec position;
};

my_vec add(my_vec &vector1, my_vec &vector2);
char playground[40][40] = {'.'};
character add_player(){
    character player_pers;

    player_pers.enemy = false;
    std::cout << "Введите имя вашего персоонажа" << std::endl;
    std::cin >> player_pers.name;
    std::cout << "Введите уровень здоровья и броню персоонажа" << std::endl;
    std::cin >> player_pers.health >> player_pers.armor;
    std::cout << "Введите урон персоонажа" << std::endl;
    std::cin >> player_pers.damage;

    return player_pers;
}

void print_field(){
    for(int i = 0; i < 40; i++){
        for (int j = 0; j < 40 ;j++)
            std::cout << playground[i][j];
        std::cout << std::endl;
    }
}
bool good_pos(my_vec &vec){
    if (vec.x < 0 || vec.x > 39) return false;
    if (vec.y < 0 || vec.y > 39) return false;
    return true;
}

std::vector<character> add_enemy(int num){
    character enemy;
    std::vector<character> all_enemy;


    for(int i = 0; i < num; i++){
        std::stringstream ss;
        //sprintf(enemy.name.c_str(), "Enemy #%d", i);
        //Более простые варианты
        //enemy.name = "Enemy #" + std::toString(i);
        //enemy.name = "Enemy #" + std::string::to_string(i);
        ss << "Enemy #" << i;
        enemy.name = ss.str();
        enemy.health = (std::rand() % 101) + 50;
        enemy.armor = (std::rand() % 51);
        enemy.damage = (std::rand() % 16) + 15;
        enemy.enemy = true;
        all_enemy.push_back(enemy);
        ss.str().clear();
    }
    return all_enemy;
}

bool rand_stand(std::vector<character> &persons){
    //int pos_x, pos_y;
    my_vec tmp_pos;

    for(int i = 0; i < 40; i++){
        for(int j = 0; j < 40; j++)
            playground[i][j] = '.';
    }

    //for(character &pers: persons){
    for(int i = 0; i < persons.size(); i++){
        tmp_pos.x = (std::rand() % 40);
        tmp_pos.y = (std::rand() % 40);

        while(playground[tmp_pos.x][tmp_pos.y] != '.'){
            //std::cout << "New position for " << persons[i].name << std::endl;
            tmp_pos.x = (std::rand() % 40);
            tmp_pos.y = (std::rand() % 40);
        }
        playground[tmp_pos.x][tmp_pos.y] = (( persons[i].enemy ) ? 'E' : 'P');
        persons[i].position = tmp_pos;
        //std::cout << "Add person " << persons[i].name << std::endl;
    }
    //print_field();
    return true;
}

bool player_die(character &pers, character &enemy){

    pers.armor -=enemy.damage;
    if(pers.armor < 0) {
        pers.health += pers.armor;
        pers.armor = 0;
    }
    if(pers.health < 1) return false;
    enemy.armor -= pers.damage;
    if(enemy.armor < 0) {
        enemy.health += enemy.armor;
        enemy.armor = 0;
    }

    if(enemy.health < 1) {
        std::cout << enemy.name << "убит" << std::endl;
        playground[enemy.position.x][enemy.position.y] = '.';
    }
}

void save_game(std::vector<character> &persons){
    std::string str;
    std::ofstream out_file;

    std::cout << "Введите имя файла для сохранения" << std::endl;
    std::cin >> str;
    out_file.open(str.c_str(), std::ios_base::binary);
    if(!out_file.is_open()) {
        std::cout << "Файл не может быть открыт" << std::endl;
        return;
    }
    int len = persons.size();
    out_file.write((char*) &len, sizeof(len));
    for(int i = 0; i < persons.size(); i++){
        len = persons[i].name.length();
        out_file.write((char*) &len, sizeof(len));
        out_file.write(persons[i].name.c_str(), len);
        out_file.write((char*) &persons[i].armor, sizeof(persons[i].armor));
        out_file.write((char*) &persons[i].health, sizeof(persons[i].health));
        out_file.write((char*) &persons[i].damage, sizeof(persons[i].damage));
        out_file.write((char*) &persons[i].enemy, sizeof(persons[i].enemy));
        out_file.write((char*) &persons[i].position.x, sizeof(persons[i].position.x));
        out_file.write((char*) &persons[i].position.y, sizeof(persons[i].position.y));
    }
    out_file.close();
}

std::vector<character> load_game(){
    std::vector<character> load_pers;
    std::ifstream in_file;
    std::string str;
    character cur_pers;
    int pers_num, len;

    std::cout << "Введите название файла для загрузки" << std::endl;
    std::cin >> str;
    in_file.open(str.c_str());
    if(!in_file.is_open()){
        std::cout << "Файл не может быть открыт" << std::endl;
        return load_pers;
    }
    for(int i = 0;i < 40; i++)
        for(int j = 0; j < 40; j++) playground[i][j] = '.';

    in_file.read((char*)&pers_num, sizeof (pers_num));
    //std::cout << "111111 pers_num: " << (int)pers_num << std::endl;
    for(int i = 0; i < pers_num; i++){
        in_file.read((char*) &len, sizeof(len));
        cur_pers.name.resize(len);
        in_file.read((char*) cur_pers.name.c_str(), len);
        in_file.read((char*) &cur_pers.armor, sizeof(cur_pers.armor));
        in_file.read((char*) &cur_pers.health, sizeof(cur_pers.health));
        in_file.read((char*) &cur_pers.damage, sizeof(cur_pers.damage));
        in_file.read((char*) &cur_pers.enemy, sizeof(cur_pers.enemy));
        in_file.read((char*) &cur_pers.position.x, sizeof(cur_pers.position.x));
        in_file.read((char*) &cur_pers.position.y, sizeof(cur_pers.position.y));
        playground[cur_pers.position.x][cur_pers.position.y] = cur_pers.enemy ? 'E' : 'P';
    }
    in_file.close();

    return load_pers;
}

bool play_round(std::vector<character> &persons, my_vec player_direct){
    my_vec tmp_pos = {-1,-1};

    for(int i = 0; i < persons.size(); i++){
        if(!persons[i].enemy) tmp_pos = add(persons[i].position, player_direct);
        else{
            tmp_pos.x = std::rand()%2;
            tmp_pos.y = std::rand()%2;
            tmp_pos = add(persons[i].position, tmp_pos);
        }
        if(!good_pos(tmp_pos)) continue;
        if(playground[tmp_pos.x][tmp_pos.y] != '.'){
            switch (persons[i].enemy){
                case true:
                    playground[persons[i].position.x][persons[i].position.y] = '.';
                    if(playground[tmp_pos.x][tmp_pos.y] == 'E') continue;
                case false:
                    for(int j = 0; j < persons.size(); j++) {
                        if(persons[j].enemy) continue;
                        return !player_die(persons[j], persons[i]);
                    }
                default:
                    break;
            }
        }
    }
    return false;
};

void sol4(){
    std::string ans;
    //int enemy_num = 5;
    std::vector<character> persons = add_enemy(5);
    bool finish = false;
    my_vec tmp_direct;

    //character player_per = add_player();
    std::cout << "Введите load если хотите загрузить сохранённые данные и new если хотите создать нового персоонажа" << std::endl;
    std::cin >> ans;
    if( ans == "load" ){
        persons = load_game();
        if(persons.empty()) std::cout << "Даннные не загружены" << std::endl;
        else std::cout << "Даннные загружены" << std::endl;
        print_field();
    }
    else{
        persons.push_back(add_player());
        rand_stand(persons);
    }
    while (!finish){
        std::cout << "Введите направление перемещения игрока(left, right, top, bottom ), save для сохранения игры или stop для завершения" << std::endl;
        std::cin >> ans;
        if( ans == "left") tmp_direct = {0, 1};
        else if( ans == "right") tmp_direct = {0, -1};
        else if( ans == "top" ) tmp_direct = {0, 1};
        else if( ans == "bottom" ) tmp_direct =  {0, -1};
        else if( ans == "save" ){
            save_game(persons);
            std::cout << "Данные сохранены" << std::endl;
            continue;
        }
        else if(ans == "stop") return;
        else{
            std::cout << "Неизвестная комманда, повторите ввод" << std::endl;
            continue;
        }

        finish = play_round(persons, tmp_direct);
        print_field();
        //finish = true;
    }
    if (persons[persons.size() - 1].health <= 0) std::cout << "Вы проиграли" << std::endl;
    else std::cout << "Вы выйграли" << std::endl;

}