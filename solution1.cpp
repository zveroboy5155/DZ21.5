#include "./solution1.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;
struct CLIENT_DATA{
    string name, first_name; 
    int sum;
    string date;
};
string default_path = "clients.txt";

void print_struct(CLIENT_DATA &person){
    cout << person.name << " " << person.first_name << " " << person.sum << " " << person.date << endl;
}

bool read(ifstream &file){
    CLIENT_DATA person;

    while(!file.eof()){
      file >> person.name >> person.first_name >> person.sum >> person.date;
      if (file.eof()) break;
      print_struct(person);
    }
    return true;
}

bool add_client(CLIENT_DATA &person, ofstream &file){

    file << person.first_name << " " << person.name << " " << person.sum << " " << person.date << endl;

    file.close();
    return true;
}

void sol1(){
    CLIENT_DATA person;
    string command, path;

    cout << "Введите путь к файлу с БД или default для использования пути "<< default_path << endl;
    cin >> path;
    if( path == "default") path = default_path;
    while(command != "stop"){
        cout<< "Введите операцию для действия с ведомостью (read или add) или stop для завершения работы программы" <<endl;
        cin >> command;
        if(command == "read") {
            ifstream in_file(path.c_str());

            if(!in_file.is_open()){
                cout << "File " << path << "not found" << endl;
                break;
            }
            read(in_file);
        }
        else if(command == "add"){
            ofstream out_file(path.c_str(), ios::app);

            if(!out_file.is_open()){
                cout<<"File " << path << "not found" <<endl;
                break;
            }
            cout <<"Введите имя фамилию сумму и дату выплаты" << endl;
            cin >> person.name >> person.first_name >> person.sum >> person.date;
            add_client(person, out_file);
        }
    }




}