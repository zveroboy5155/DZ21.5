#include <iostream>

#include "solution3.h"
#include <cmath>
#include <string>

struct my_vec{
    double x, y;
};
my_vec add(my_vec &vector1, my_vec &vector2);
my_vec subtract(my_vec &vector1, my_vec &vector2);
my_vec scale(my_vec &vector1, int cons);
int length(my_vec &vector1);
my_vec normalize(my_vec &vector1);

//Сложение двух векторов — команда add
my_vec add(my_vec &vector1, my_vec &vector2){
    my_vec res;

    res.x = vector1.x + vector2.x;
    res.y = vector1.y + vector2.y;

    return res;
}

//Вычитание двух векторов — команда subtract
my_vec subtract(my_vec &vector1, my_vec &vector2){
    my_vec res;

    res.x = vector1.x - vector2.x;
    res.y = vector1.y - vector2.y;
    return res;
}
//Умножение вектора на скаляр — команда scale.
my_vec scale(my_vec &vector1, int cons){
    my_vec res;
    res.x = vector1.x * cons;
    res.y = vector1.y * cons;
    return res;
}
//Нахождение длины вектора — команда length.
int length(my_vec &vector1){
    return sqrt(vector1.x * vector1.x + vector1.y * vector1.y);;
}
//Нормализация вектора — команда normalize.
my_vec normalize(my_vec &vector1){
    my_vec res;
    res.x = vector1.x/ length(vector1);
    res.y = vector1.y/ length(vector1);
    return res;
}

void sol3(){
    //std::cout << "Задача 3 ещё не решена" << std::endl;
    my_vec in_vector, res;
    std::string ans;

    std::cout<< "Введите координаты входного вектора(x и y)"<< std::endl;
    std::cin >> in_vector.x >>in_vector.y;
    std::cout<< "Введите операцию которую хотите произвести(add, subtract, scale, normalize, length)"<< std::endl;
    std::cin >> ans;

    if(ans == "add") {
        my_vec second_vector;
        std::cout<< "Введите координаты второго вектора(x и y)"<< std::endl;
        std::cin >> second_vector.x >> second_vector.y;
        res = add(in_vector,second_vector);
    }
    else if(ans == "subtract"){
        my_vec second_vector;
        std::cout<< "Введите координаты второго вектора(x и y)"<< std::endl;
        std::cin >> second_vector.x >> second_vector.y;
        res = subtract(in_vector,second_vector);
    }
    else if(ans == "subtract"){
        int cons;
        std::cout<< "Введите скалярное значение на которое нужно умножить вектор"<< std::endl;
        std::cin >> cons;
        res = scale(in_vector, cons);
    }
    else if (ans == "normalize")
        res = normalize(in_vector);
    else if (ans == "length")
        std::cout << "Длинна вектора равна: " << length(in_vector) << std::endl;
    else {
        std::cout << "Неверная комманда" << std::endl;
        return;
    }
    if (ans != "length") std::cout << "Результирующий вектор имеет координаты: "<< res.x<< " " << res.y << std::endl;
}